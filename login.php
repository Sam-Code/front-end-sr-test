<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">  
	    <title>Login</title>
	    <link rel="stylesheet" href="css/bootstrap.min.css" />
	    <link rel="stylesheet" href="css/login.css" />

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>
	<body>    
		<div class="container">           
	        <div class="col-lg-3"></div>
			<div class="col-sm-12 col-lg-6" style="margin-top:2%">    				            
	            <h3 class="text-center" style="color:white;padding-bottom:30px;"> ¡Bienvenido a Kontrol! </h3>
				<div style="background:white;">
		            <img style="padding: 20px 5px 5px 5px;" class="img-responsive center-block" src="img/logo_login.png" />
		            <hr>
		            <div style="padding: 30px;">
			            <form id="login-form" action="#" method="post" role="form">
							<div class="form-group">
								<label class="control-label" for="email">Email</label>
								<input style="border:0px" type="text" id="email" class="form-control" name="email" autofocus="autofocus">
							</div>
							<div class="form-group" style="padding-top:30px">
								<label class="control-label" for="contrasenia">Contraseña</label>
								<input style="border:0px" type="text" id="contrasenia" class="form-control" name="contrasenia">
							</div>
							<div class="form-group" style="padding-top:30px;">
			                    <button type="submit" class="btn btn-main" name="btn-login" >ENTRAR</button>
			                </div>
			            </form>
			            <p class="text-center" style="padding: 35px 20px 0px 20px;">
			            	<a>¿Olvidaste tu contraseña?</a>
			            </p>			            
		        	</div>
	        	</div>
	        	
	        	<p class="text-center" style="padding: 35px 20px 0px 20px;">
	            	<span class="font-purple">¿ No tienes cuenta ?</span> <a>registrate aquí</a>
	            </p>	            
			</div>
			<div class="col-lg-3"></div>						
	    </div>
	    <div class="container-fluid">
		    <div class="col-lg-12">
				<p style="color:#9B9C9E;padding-right:50px;" class="text-right">
					&copy; Kichink 2015
				</p>
			</div>
		</div>
	</body>
</html>
