<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">  
	    <title>Dashboard</title>
	    <link rel="stylesheet" href="css/bootstrap.min.css" />
	    <link rel="stylesheet" href="css/dashboard.css" />
      <link rel="stylesheet" href="css/font-awesome.min.css">

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>
	<body>

    <nav class="navbar navbar-top" style="z-index:99" style="">
      <div class="container-fluid"> 

        <div class="navbar-header" >
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar icon-bar-purple"></span>
            <span class="icon-bar icon-bar-purple"></span>
            <span class="icon-bar icon-bar-purple"></span>
          </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar" >    		
          <ul class="nav navbar-nav navbar-right" >
      			<li><a href="#">Saldo <span class="font-purple">$8.250</span></a></li>
      			<li class="divider-vertical"></li>
      			<li >
      				<a href="#" style="">
      					¡Hola <span class="font-purple">Kra</span>
      				</a>
      			</li>
            <li>
              <a><img src="img/logo_user.png" /> </a>
            </li>
            <li class="dropdown">
                <a class="btn dropdown-toggle" type="button" id="dropdownMenu1" 
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  <span class="glyphicon purple glyphicon-chevron-down" aria-hidden="true"></span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                  <li><a href="#">Configuración</a></li>
                  <li><a href="#">Ayuda</a></li>
                  <li><a href="#">Salir</a></li>
                </ul>
            </li>
            <li></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar" style="margin-top:-51px;z-index:100;background:#542581;border:0px;">        	        	
          <div class="text-center">
        		<img src="img/logo_dashboard.png" />        		
        	</div>
        	<div class="text-center" style="padding-top:30px">
        		<img src="img/logo_user.png" />        		
        	</div>
        	<p style="color:white;padding:30px 10px 20px 10px;">
        		Kraken piezas unicas		
        	</p>
    			<ul class="nav nav-sidebar nav-sidebar-purple" style="background:#451F6C;" >
            <li class="active">
              <a href="#" > 
                <i class="fa fa-home" style="padding-right: 10px;" aria-hidden="true"></i>
                <span> Home <span>
                <span class="badge pull-right badge-green" >4</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-shopping-bag" style="padding-right: 10px;" aria-hidden="true"></i>
                Articulos</a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-shopping-cart" style="padding-right: 10px;" aria-hidden="true"></i>
                Ordenes <span class="badge pull-right badge-green">1</span></a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-tag" style="padding-right: 10px;" aria-hidden="true"></i>
                Categorias</a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-credit-card" style="padding-right: 10px;" aria-hidden="true"></i>
                Pagos</a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-inbox" style="padding-right: 10px;" aria-hidden="true"></i>
                Mensajes</a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-bar-chart" style="padding-right: 10px;" aria-hidden="true"></i>
                Reportes</a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-gear" style="padding-right: 10px;" aria-hidden="true"></i>
                Austes</a>
            </li>
    			</ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="background:#E3E4E8">
          <h1 class="page-header">Dashboard</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
          </div>

          <h2 class="sub-header">Section title</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Header</th>
                  <th>Header</th>
                  <th>Header</th>
                  <th>Header</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td>sit</td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>Integer</td>
                  <td>nec</td>
                  <td>odio</td>
                  <td>Praesent</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>libero</td>
                  <td>Sed</td>
                  <td>cursus</td>
                  <td>ante</td>
                </tr>
                <tr>
                  <td>1,004</td>
                  <td>dapibus</td>
                  <td>diam</td>
                  <td>Sed</td>
                  <td>nisi</td>
                </tr>
                <tr>
                  <td>1,005</td>
                  <td>Nulla</td>
                  <td>quis</td>
                  <td>sem</td>
                  <td>at</td>
                </tr>
                <tr>
                  <td>1,006</td>
                  <td>nibh</td>
                  <td>elementum</td>
                  <td>imperdiet</td>
                  <td>Duis</td>
                </tr>
                <tr>
                  <td>1,007</td>
                  <td>sagittis</td>
                  <td>ipsum</td>
                  <td>Praesent</td>
                  <td>mauris</td>
                </tr>
                <tr>
                  <td>1,008</td>
                  <td>Fusce</td>
                  <td>nec</td>
                  <td>tellus</td>
                  <td>sed</td>
                </tr>
                <tr>
                  <td>1,009</td>
                  <td>augue</td>
                  <td>semper</td>
                  <td>porta</td>
                  <td>Mauris</td>
                </tr>
                <tr>
                  <td>1,010</td>
                  <td>massa</td>
                  <td>Vestibulum</td>
                  <td>lacinia</td>
                  <td>arcu</td>
                </tr>
                <tr>
                  <td>1,011</td>
                  <td>eget</td>
                  <td>nulla</td>
                  <td>Class</td>
                  <td>aptent</td>
                </tr>
                <tr>
                  <td>1,012</td>
                  <td>taciti</td>
                  <td>sociosqu</td>
                  <td>ad</td>
                  <td>litora</td>
                </tr>
                <tr>
                  <td>1,013</td>
                  <td>torquent</td>
                  <td>per</td>
                  <td>conubia</td>
                  <td>nostra</td>
                </tr>
                <tr>
                  <td>1,014</td>
                  <td>per</td>
                  <td>inceptos</td>
                  <td>himenaeos</td>
                  <td>Curabitur</td>
                </tr>
                <tr>
                  <td>1,015</td>
                  <td>sodales</td>
                  <td>ligula</td>
                  <td>in</td>
                  <td>libero</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
    <script src="js/bootstrap.min.js"></script>
  

</body> 
		
	</body>
</html>